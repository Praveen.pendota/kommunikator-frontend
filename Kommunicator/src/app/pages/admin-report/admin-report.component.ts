import { Component, OnInit } from '@angular/core';
import {AdminReportService} from '../../services/admin-report-service/admin-report.service';
import {Chart} from '../../../../node_modules/chart.js'; 
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { ResponseService } from 'src/app/services/response/response.service';
import { map } from 'rxjs/operators';
import { MessageService } from 'src/app/services/create-message/message.service';
import { WHITE_ON_BLACK_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
@Component({
  selector: 'app-admin-report',
  templateUrl: './admin-report.component.html',
  styleUrls: ['./admin-report.component.scss']
})
export class AdminReportComponent implements OnInit {

  underTitle:string=""
  showUser:boolean=false;
  showCategories:boolean=false;
  showTopics:boolean=false;
  showSubscriptions:boolean=false;
  showMessages:boolean=false;
  showMessagesText:boolean=false;

  showTable:boolean=false;
  showText:boolean=false;

  topics:string []=[];
  users:string []=[];
  categories:string []=[];
  subscriptions:string []=[];

  subscriptionsByTopic:string[]=[];
  nameOfTopic:string[]=[];

  messageIds:any []=[];
  responseLikert:string[]=[];
  responseText:string[]=[];
  nameOfMessage:any[]=[];
  nameOfMessageInput=[];
  nameOfMessageInputText=[];



  nameMapping:any = [];

  arrayOfAllMessageNames:string[]=[];
  constructor(private adminService:AdminReportService, private responseService:ResponseService,private messageService:MessageService) { }



  ngOnInit(): void {
    this.getCategories();
    this.getTopcis();
    this.getUsers();
    this.getSubscriptions();
    this.showTable=true;
    this.drawChart([],[],[])
    
    this.messageService.fetchAllMessages().subscribe(
      x=>//console.log(x))
       x.map(message => {
         this.nameMapping[message.messageID.toString()] = message.messageTemplate.name
       })
      //this.nameOfMessage=x.map(x=> {return {messageId: x.messageID, messageName: x.messageTemplate.name}})
    )
  }
    

  getTopcis(){
    this.showTable=true;  
    this.showText=false;
    this.adminService.getNumberOfTopics().subscribe(
      topics=>{
        this.topics=topics.map(topics=>topics.name);
      },
      err=>{
        console.log(err)
      }
    )

    if(this.showTable==true){
     this.drawChart([this.topics.length],['Topics'],
     this.getRandomColor(this.categories.length));
    }
    this.showUser=false;
    this.showCategories=false;
    this.showSubscriptions=false;
    this.showMessages=false;
    this.showMessagesText=false;
  };
  
  getCategories(){
    this.showTable=true;
    this.showText=false;
    this.adminService.getNumberOfCategories().subscribe(
      categories=>{
        this.categories=categories.map(categories=>categories.name);
      },
      err=>{
        console.log(err);
      }
    )
    if(this.showTable==true){
    this.drawChart([this.categories.length],['Categories'],
    this.getRandomColor(this.categories.length));
    }
    this.showUser=false;
    this.showTopics=false;
    this.showSubscriptions=false;
    this.showMessages=false;
    this.showMessagesText=false;

  };
  getUsers(){
    this.showTable=true;
    this.showText=false;
    this.adminService.getNumberOfSubscribers().subscribe(
      users=>{
        console.log(users);
        this.users=users.map(users=>users.userEmail);
        console.log(this.users);
      },
      err=>{
        console.log(err);
      }
    )
    if(this.showTable==true){
    this.drawChart([this.users.length],['Users'],
    this.getRandomColor(this.users.length));
    }
    this.showTopics=false;
    this.showCategories=false;
    this.showSubscriptions=false;
    this.showMessages=false;
    this.showMessagesText=false;

  }
  
  getSubscriptions(){
    this.showTable=true;
    this.showText=false;
    this.adminService.getSubscriptions().subscribe(
      x=>{
        this.subscriptionsByTopic=x
        .map(users=>users.topicSubscriptions.length)
        this.nameOfTopic=x.map(name=>name.name)
      });
      if(this.showTable==true){
        this.drawChart(this.subscriptionsByTopic,this.nameOfTopic,
        this.getRandomColor(this.subscriptionsByTopic.length));
        }
        this.showUser=false;
        this.showTopics=false;
        this.showCategories=false;
        this.showMessages=false;
        this.showMessagesText=false;

  }
  /*If I get the time, add functionality to display some more*/ 
  getResponses(){
    this.showTable=true;
    this.showText=false;
    this.responseLikert=[];
    this.messageIds=[];
    this.nameOfMessageInput=[];
    this.responseService.getResponse().subscribe(
      res=>{
        res.map(response => {
          const responseContent = JSON.parse(response.responseContent);
          if (responseContent.responseType == 'likert') {
            responseContent.messageId = response.messageId;
            this.responseLikert.push(responseContent.responseValue);
            this.messageIds.push(responseContent.messageId);
            this.nameOfMessageInput.push(this.nameMapping[responseContent.messageId.toString()])
            if(this.showTable==true){
              this.drawChart(this.responseLikert,this.nameOfMessageInput,
              this.getRandomColor(this.messageIds.length));
              }
          }
        })
      }
    ); 
             this.showTopics=false;
            this.showCategories=false;
            this.showSubscriptions=false;
            this.showUser=false; 
            this.showMessagesText=false;           
    }
    
    getTextResponse(){
      this.showTable=false;
      this.showText=true;
      this.responseText=[];
      this.messageIds=[];
      this.nameOfMessageInputText=[];
      this.responseService.getResponse().subscribe(
        res=>{
            console.log(res);
          res.map(response => {
            const responseContent = JSON.parse(response.responseContent);
            if (responseContent.responseType == 'text') {
              responseContent.messageId = response.messageId;
              this.responseText.push(responseContent.responseValue);
              this.messageIds.push(responseContent.messageId);
              this.nameOfMessageInputText.push(this.nameMapping[responseContent.messageId.toString()])
              this.showTopics=false;
              this.showCategories=false;
              this.showSubscriptions=false;
              this.showUser=false;
              this.showMessages=false;
            }
          })
        }
      ); 
      console.log(this.nameOfMessageInputText);
      console.log(this.responseText)
      }

    
  
  chooseWhatToExport(){
    if(this.showTopics!=true &&this.showUser!=true && this.showCategories!=true && this.showSubscriptions!=true &&this.showMessages!=true && this.showMessagesText!=true){
      alert("Choose what to export");
    }
    else{
      const saveFileAs=prompt("Enter a filename");
      if(saveFileAs=='' || saveFileAs==null){
        const saveFileAs=prompt("You must enter a valid filename")
      }
      else if(this.showUser==true){
        this.exportToCSV(this.users,saveFileAs);
      }
      else if(this.showTopics==true){
          this.exportToCSV(this.topics,saveFileAs);
      }
      else if(this.showCategories==true){
          this.exportToCSV(this.categories,saveFileAs)
      }
      else if(this.showSubscriptions==true){
        this.exportToCSV( this.nestTwoArraysForCsv(this.subscriptionsByTopic,this.nameOfTopic),saveFileAs);
      }
      else if(this.showMessages==true){
        this.exportToCSV(this.nestTwoArraysForCsv(this.nameOfMessageInput,this.responseLikert),saveFileAs);
      }
      else if(this.showMessagesText==true){
        this.exportToCSV(this.nestTwoArraysForCsv(this.nameOfMessageInputText,this.responseText),saveFileAs);
      }
    }
    
   
  }
   exportToCSV(data,saveFileAs){
     saveFileAs+=".csv";
     var csv=data.join(',');
     let blobCSV=new Blob([csv],{type:"text/csv"});
     this.download(blobCSV,saveFileAs)
    }
    
    download(blob, filename){
      const url=window.URL.createObjectURL(blob);
      const a=document.createElement("a");
      a.href=url;
      a.download=filename;
      a.click();
    }


  drawChart(dataToBedisplayed, arrayOfLabels,arrayOfcolor){
    if(this.showTable==true || this.showCategories==true || this.showUser==true){
     var myChart = new Chart("myChart", {
      type: 'bar',
      data: {
          labels:arrayOfLabels,
          datasets: [{
              data: dataToBedisplayed,
              backgroundColor: arrayOfcolor,
              borderColor: [
              ],
              borderWidth: 1
          }]
      },
      
      options: {
        events: [],
        responsive:true,
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true,
                      fontColor:"white"
                  },gridLines: {
                    color: "white"
                  }
              }],
              xAxes:[{
                ticks:{
                  fontColor:"white"
                }
                
              }]
          },
          legend: {
            display: false,
            labels:{
              fontColor: 'red',
              fontSize:10,
            }
          },
          
      }
  });
}
  }
 
    getRandomColor(lengthOfVariable) {
      let arrayOfColors:string[]=[]
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for(let i=0;i<lengthOfVariable;i++){
        for (let j = 0; j< 6; j++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
        arrayOfColors.push(color);
        color='#';
      }
      return arrayOfColors;
  }
  nestTwoArraysForCsv(array1,array2){
    let i=0
    let nestedArray:string[]=[]
    while(i<array1.length){
      nestedArray.push(array1[i]);
      nestedArray.push(array2[i]);
      i++;
    }
    return nestedArray;
  }
    
  
  
}
