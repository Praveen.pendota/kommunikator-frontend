import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageTemplateSidebarComponent } from './message-template-sidebar.component';

describe('MessageTemplateSidebarComponent', () => {
  let component: MessageTemplateSidebarComponent;
  let fixture: ComponentFixture<MessageTemplateSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessageTemplateSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageTemplateSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
