import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { responseTemplate } from 'src/models/responseTemplate';
import { BackendService } from '../backend.service';


@Injectable({
  providedIn: 'root'
})


export class ResponseTemplateService {

  constructor(private http: BackendService) { }

  private dataModelPassedToMultiple=new BehaviorSubject<any>("Choose A Reponse Template");
  curentDataModel=this.dataModelPassedToMultiple.asObservable();

  private DataForContent=new BehaviorSubject<any>("Please Choose a Response template");
  curentContentDataModel=this.DataForContent.asObservable();

  changeDataModel(name: string){
    this.dataModelPassedToMultiple.next(name);
  }

  changeContentDataModel(content: string) {
    this.DataForContent.next(content)
  }

  SaveResponseTemplate(responseTemplate: responseTemplate): Observable<responseTemplate> {
    return this.http.post<responseTemplate>('api/ResponseTemplates', {
      responseTemplateID: responseTemplate.responseTemplateID,
      responseType: responseTemplate.responseType,
      name: responseTemplate.name,
      content: responseTemplate.content
    })
  }

  getAllResponseTemplates(): Observable<responseTemplate[]> {
    return this.http.get<responseTemplate[]>('api/ResponseTemplates')
  }

  getResponseTemplate(responseTemplateId: number): Observable<responseTemplate> {
    return this.http.get('api/ResponseTemplates/' + responseTemplateId);
  }

  deleteResponse(res: responseTemplate) {
    return this.http.delete<responseTemplate>(`api/ResponseTemplates/${res.responseTemplateID}`)
    
  }


}
