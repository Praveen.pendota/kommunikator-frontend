import { Component, Input, OnInit } from '@angular/core';
import { Category } from 'src/models/category.model';
import { CategoryService } from 'src/app/services/category/category.service';


@Component({
  selector: 'app-response-template',
  templateUrl: './response-template.component.html',
  styleUrls: ['./response-template.component.scss']
})
export class ResponseTemplateComponent implements OnInit {


  categories: Category[] = [];
  templateComponent = "";

  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.categoryService.fetchAllCategories().subscribe((res) => {
      this.categories = res;
      console.log(this.categories)
    })
  }

  public ChangeComponent(event):void {
    this.templateComponent = event;
    console.log(this.templateComponent)
  }

  doSomething() {
    console.log('working')
  }

}
