import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class AdminReportService {

  constructor(private http: BackendService) { }
  getNumberOfSubscribers(): Observable<any[]> {
    return this.http.get<any[]>('api/users');
  }
  getNumberOfTopics():Observable<any[]>{
    return this.http.get<any[]>('api/topics');
  }
  getNumberOfCategories():Observable<any[]>{
    return this.http.get<any[]>('api/categories');
  }
  getSubscriptions():Observable<any[]>{
    return this.http.get<any[]>('api/topics');
  }
  getUsers():Observable<any[]>{
    return this.http.get<any[]>('api/users');
  }
}
