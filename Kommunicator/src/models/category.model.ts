import { SubCategory } from './subcategory.model';


export interface Category {
    id?: number,
    name: string,
    active?: boolean,
    subCategories?: SubCategory[]
}