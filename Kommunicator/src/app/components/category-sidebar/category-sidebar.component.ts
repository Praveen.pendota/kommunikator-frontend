import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/models/category.model';
import { SubCategory } from 'src/models/subcategory.model';

@Component({
  selector: 'app-category-sidebar',
  templateUrl: './category-sidebar.component.html',
  styleUrls: ['./category-sidebar.component.scss']
})

export class CategorySidebarComponent implements OnInit {
  @Input() editable: boolean;
  @Input() categories: Category[];

  @Output() newCategoryClick?: EventEmitter<any> = new EventEmitter<Category>();
  @Output() newSubCategoryClick?: EventEmitter<any> = new EventEmitter<SubCategory>();
  @Output() addCategory = new EventEmitter<any>();

  @Output() editCategoryClick?: EventEmitter<any> = new EventEmitter<Category>();
  @Output() editSubCategoryClick?: EventEmitter<any> = new EventEmitter<SubCategory>();

  @Output() deleteCategoryClick?: EventEmitter<any> = new EventEmitter<number>();
  @Output() deleteSubCategoryClick?: EventEmitter<any> = new EventEmitter<SubCategory>();
  
  newCategoryName: string;
  indexedNewSubCategories: any = {};

  @Output() messageEvent = new EventEmitter<string>();

  @Output() AllTopic = new EventEmitter<string>();
  
  message: string;

  @Input() seeSubscriptions: boolean;
  
  constructor() { 
  }
  ngOnInit(): void {
    
  }

  sendMessage(message) {
    this.messageEvent.emit(message)
  }

  displayAll() {
    this.AllTopic.emit()
  }

  addCategories() {
    this.addCategory.emit(console.log(1))
  }

  onNewCategoryClicked() {
    const newCategory: Category = {
      name: this.newCategoryName,
      active: true
    };
    this.newCategoryName = "";
    this.newCategoryClick.emit(newCategory);
  }

  onNewSubCategoryClicked(categoryId: number) {
    const newSubCategory: SubCategory = {
      categoryID: categoryId,
      name: this.indexedNewSubCategories[categoryId.toString()],
      active: true
    };
    this.indexedNewSubCategories[categoryId.toString()] = "";
    this.newSubCategoryClick.emit(newSubCategory);
  }


  onEditCategoryClicked(categoryId: number) {
    const newCategoryNameInput = prompt("Enter new category name:");
    if (newCategoryNameInput != null) {
      const editedCategory: Category = {
        id: categoryId,
        name: newCategoryNameInput,
        active: true
      };
      this.editCategoryClick.emit(editedCategory);
    }
  }

  onEditSubCategoryClicked(subCategory: SubCategory) {
    const newSubCategoryNameInput = prompt("Enter new subcategory name:");
    if (newSubCategoryNameInput != null) {
      const editedSubCategory: SubCategory = {
        id: subCategory.id,
        categoryID: subCategory.categoryID,
        name: newSubCategoryNameInput,
        active: true
      };
      this.editSubCategoryClick.emit(editedSubCategory);
    }
    
  }
  onDeleteCategoryClicked(categoryId: number) {
    if (confirm("This will delete all related subcategories and topics.")) {
      this.deleteCategoryClick.emit(categoryId);
    }
    
  }

  onDeleteSubCategoryClicked(subCategory: SubCategory) {
    if (confirm("This will delete all related topics.")) {
      this.deleteSubCategoryClick.emit(subCategory);
    }
    
  }

}
