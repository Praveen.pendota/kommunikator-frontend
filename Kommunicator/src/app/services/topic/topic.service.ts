import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Topic } from 'src/models/topic.model';
import { Observable } from 'rxjs';
import { BackendService } from '../backend.service';

@Injectable({
    providedIn: 'root'
  })
  export class TopicService {
  
    constructor(private http: HttpClient, private backend: BackendService) { }

    _fetchAllTopics(): Observable<Topic[]> {
      return this.backend.get<Topic[]>("api/Topics");
    }
  
    fetchAllTopics(): Observable<Topic[]> {
      return this.backend.get<Topic[]>('api/Topics');
    }

    addTopic(topic: Topic): Observable<Topic> {
      return this.backend.post<Topic>('api/Topics', topic);
    }

    editTopic(topic: Topic): Observable<Topic> {
      return this.backend.put<Topic>('api/Topics/' + topic.id, topic);
    }

    deleteTopic(topic: Topic): Observable<Topic>{
      return this.backend.put('api/Topics/delete/' + topic.id, topic);
    }
  }