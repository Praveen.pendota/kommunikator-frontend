import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category/category.service';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';
import { TopicService } from 'src/app/services/topic/topic.service';
import { environment } from 'src/environments/environment';
import { Message } from 'src/models/message';
import { responseTemplate } from 'src/models/responseTemplate';

enum ResponseType {
  TextResponse,
  RatingResponse
}

@Component({
  selector: 'app-create-response-template',
  templateUrl: './create-response-template.component.html',
  styleUrls: ['./create-response-template.component.scss']
})

export class CreateResponseTemplateComponent implements OnInit {
  //API key to use the TINY MCE
  API_KEY_MCE=environment.API_KEY_TINYMCE;
 
  dataModel:any="helloworld";
  index:any;

  // Selected Response Type
  responseType: ResponseType = ResponseType.TextResponse;

  
  //Model for loading the datamodel
  @Input() Content:string = "hello";
  @Input() TemplateName: string = "hello";

  @Input() Message?: responseTemplate;

  @Output() ResponseTemplateId = new EventEmitter();
  @Output() ChosenResponseType = new EventEmitter();

  constructor(private ResponseService: ResponseTemplateService) { }

  get ResponseTypes(): typeof ResponseType
  {
    return ResponseType;
  }

  ngOnInit(): void {
    this.ResponseService.curentDataModel.subscribe(message => this.TemplateName = message)
    this.ResponseService.curentContentDataModel.subscribe(message => this.Content = message)
  }

  receiveMessage(event) {
    console.log("this is the event object:", event)
    this.Message = event;
    this.ResponseService.changeDataModel(event.name)
    this.ResponseService.changeContentDataModel(event.content)

    this.ResponseTemplateId.emit(event.responseTemplateID);
  }

  async saveResponseTemplate() {
    return new Promise(resolve => {
      this.Message.content = this.Content
      this.Message.name = this.TemplateName
      this.Message.responseType = this.responseType
    
      this.ResponseService.SaveResponseTemplate(this.Message).subscribe((user) => {
        this.ResponseTemplateId.emit(user.responseTemplateID);
        resolve();
      });
    })
  }

  deleteResponseTemplate() {
    let res = this.Message
    this.ResponseService.deleteResponse(res).subscribe((res) => {
      console.log('working delete')
    })
    
    location.reload()
    
  }

  changeTemplateName(event) {
    this.TemplateName = event.target.value;
  }

  changeResponse(type: ResponseType) {
    this.responseType = type;
    this.Message.responseType = type;
    this.ChosenResponseType.emit(ResponseType[type]);
  }
}
