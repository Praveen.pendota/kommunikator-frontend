import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalSubscriptionsComponent } from './personal-subscriptions.component';

describe('PersonalSubscriptionsComponent', () => {
  let component: PersonalSubscriptionsComponent;
  let fixture: ComponentFixture<PersonalSubscriptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalSubscriptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalSubscriptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
