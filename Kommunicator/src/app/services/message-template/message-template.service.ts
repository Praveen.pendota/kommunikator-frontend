import { HttpClient } from '@angular/common/http';
import { importExpr } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Message } from 'src/models/message';
import { environment } from '../../../environments/environment';
import { MessageTemplate } from '../../../models/MessageTemplate'
import { BackendService } from '../backend.service';


@Injectable({
  providedIn: 'root'
})
export class MessageTemplateService {


  constructor(
    private http: BackendService) { }


  private dataModelPassedToMultiple = new BehaviorSubject<any>("Template Name");
  curentDataModel = this.dataModelPassedToMultiple.asObservable();

  private DataForContent = new BehaviorSubject<any>("Template Content");
  curentContentDataModel = this.DataForContent.asObservable();

  changeDataModel(message: any) {
    this.dataModelPassedToMultiple.next(message);
  }
  changeDataForMessageTemplate(message:any){
    //this.dataModelForMessageTemplate.next(message);
  }

  changeContentDataModel(content: string) {
    this.DataForContent.next(content)
  }

  getMessageTemplates(): Observable<any[]> {
    return this.http.get<any[]>(environment.API_GET_COMPONENTS);
  }

  postMessageTemplates(messageTemplate: any) {
    this.http.post<any>(environment.API_POST_COMPONENTS, messageTemplate).subscribe(
      (data: any) => {
        console.log(data)
      }
    )
  }

  GetAllMessages(): Observable<MessageTemplate[]> {
    return this.http.get<MessageTemplate[]>('api/MessageTemplates');
  }

  SaveMessageTemplate(MessageTemplate: MessageTemplate): Observable<MessageTemplate> {
    return this.http.post<MessageTemplate>('api/MessageTemplates', {
      id: MessageTemplate.id,
      name: MessageTemplate.name,
      content: MessageTemplate.content
    })

  }

  getAllMessageTemplates(): Observable<MessageTemplate[]> {
    return this.http.get<MessageTemplate[]>('api/MessageTemplates')
  }

  deleteMessage(res: MessageTemplate) {
    return this.http.delete<MessageTemplate>(`api/MessageTemplates/${res.id}`)

  }
}
