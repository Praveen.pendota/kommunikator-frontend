import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import {MessageTemplateService} from '../../services/message-template/message-template.service';

@Component({
  selector: 'app-saved-message-templates',
  templateUrl: './saved-message-templates.component.html',
  styleUrls: ['./saved-message-templates.component.scss']
})
export class SavedMessageTemplatesComponent implements OnInit {

  constructor(private messageTemplateService:MessageTemplateService, private router:Router) { }

  public API_KEY_MCE=environment.API_KEY_TINYMCE;

  //List of saved templates loaded from the database
  listOfSavedTemplates: any[] =[]
  //BOolean to show all the templates
  showAll: boolean=false;

  showListOfSavedTemplates: any[]=[]
  selectedTemplate:any;
  

  ngOnInit(): void {
    this.messageTemplateService.getMessageTemplates().subscribe(
    x=>{
      this.listOfSavedTemplates=x.map(x=>new DOMParser().parseFromString(x.inputType,"text/html")
      );
    },
    err=>{
      console.log(err);
    });

  }
  loadEarlierTemplates(){
    this.showAll=!this.showAll;
    this.showListOfSavedTemplates=[];
    this.listOfSavedTemplates.map(
      element=>this.showListOfSavedTemplates.push(element.activeElement)
    );
    console.log(this.showListOfSavedTemplates)
  }
  
  youClickedMe(x){
    /*
    this.selectedTemplate="";
    this.messageTemplateService.setOutgoingTempateData(this.showListOfSavedTemplates[x]);
    this.selectedTemplate=this.messageTemplateService.getOutgoingTemplateData();
    this.router.navigateByUrl('/selectedTemplate')
    */
      }
}
