import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseService } from 'src/app/services/response/response.service';
import { responseTemplate } from 'src/models/responseTemplate';
import { Response } from 'src/models/response.model';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';

@Component({
  selector: 'app-response-page',
  templateUrl: './response-page.component.html',
  styleUrls: ['./response-page.component.scss']
})
export class ResponsePageComponent implements OnInit {

  submitted = false;

  messageId: number;
  userEmail: string;

  urlData;

  testData = { responseTemplateId: 1, userEmail: 'magnus@gmail.com', messageId: 1 }

  responseTemplate: responseTemplate = {};
  responseTemplateContent: any;

  textResponse?: boolean;
  likertResponse?: boolean;

  textValue: string;

  likertValue: number;
  likertScale: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private responseService: ResponseService,
    private responseTemplateService: ResponseTemplateService) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.likertResponse = data.likertResponse;
      this.textResponse = data.textResponse;
    })

    this.urlData = JSON.parse(atob(this.route.snapshot.paramMap.get('data')));
    this.userEmail = this.urlData.userEmail;

    this.responseTemplateService.getResponseTemplate(this.urlData.responseTemplateId).subscribe(res => {
      this.responseTemplate = res;
      const content = new DOMParser().parseFromString(res.content, "text/html").activeElement.innerHTML;
      this.responseTemplateContent = content;
      console.log(this.responseTemplateContent);
    })
  }

  handleSubmit() {
    if (!this.submitted) {
      var response: Response = {
        userEmail: this.urlData.userEmail,
        messageId: this.urlData.messageId,
        responseContent: "",
        active: true
      };

      if (this.textResponse) {
        response.responseContent = JSON.stringify({ responseType: 'text', responseValue: this.textValue })
      } else if (this.likertResponse) {
        response.responseContent = JSON.stringify({ responseType: 'likert', responseValue: this.likertValue })
      }

      this.responseService.addResponse(response).subscribe((res) => {
        if (res) {
          alert("Your response has been recieved, thank you!");
          this.router.navigateByUrl('/user-subscriptions');
        } else {
          alert("Your message was not recieved, please try again.");
        }
      });
      this.submitted = true;
    } else {
      alert("You have already sent your submission.");
    }
  }
}
