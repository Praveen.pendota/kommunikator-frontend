import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category/category.service';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { TopicService } from 'src/app/services/topic/topic.service';
import { environment } from 'src/environments/environment';
import { Topic } from 'src/models/topic.model';

@Component({
  selector: 'app-create-template',
  templateUrl: './create-template.component.html',
  styleUrls: ['./create-template.component.scss']
})
export class CreateTemplateComponent implements OnInit {
  
  dataModel:any="helloworld";
  index:any;

  constructor(private topicService: TopicService,private categoryService: CategoryService, private router: Router,
    private messageTemplateService:MessageTemplateService) { }


  ngOnInit(): void {
    this.messageTemplateService.curentDataModel.subscribe(message=>this.dataModel=message);
  }
  recieveMessage($event){
    console.log($event.outerHTML)
    let htmlData=new DOMParser().parseFromString($event.outerHTML,"text/html");
    console.log(htmlData);
    //new DOMParser().parseFromString(x.inputType,"text/html")
    this.messageTemplateService.changeDataModel(htmlData.body.outerHTML);
  }


}
