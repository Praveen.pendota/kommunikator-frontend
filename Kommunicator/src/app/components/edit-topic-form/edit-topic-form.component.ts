import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TopicFormData } from 'src/app/pages/manage-topics/manage-topics.component';
import { Category } from 'src/models/category.model';
import { SubCategory } from 'src/models/subcategory.model';
import { Topic } from 'src/models/topic.model';

@Component({
  selector: 'app-edit-topic-form',
  templateUrl: './edit-topic-form.component.html',
  styleUrls: ['./edit-topic-form.component.scss']
})
export class EditTopicFormComponent {

  
  constructor(public dialogRef: MatDialogRef<EditTopicFormComponent>,
    @Inject(MAT_DIALOG_DATA) public topicFormData: TopicFormData) { }

}
