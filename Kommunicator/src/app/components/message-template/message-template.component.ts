import { Component, Input, OnInit } from '@angular/core';
import { Category } from 'src/models/category.model';
import { CategoryService } from 'src/app/services/category/category.service';
import {environment} from 'src/environments/environment';
import { FormControl, FormGroup } from '@angular/forms';
import { TopicsService } from 'src/app/services/topics/topics.service';
import { map } from 'rxjs/operators';
import {MessageTemplateService} from '../../services/message-template/message-template.service';
import { Router } from '@angular/router';
import { ResponseTemplateService } from 'src/app/services/response-template/response-template.service';
@Component({
  selector: 'app-message-template',
  templateUrl: './message-template.component.html',
  styleUrls: ['./message-template.component.scss']
})
export class MessageTemplateComponent implements OnInit {

  //API key to use the TINY MCE
  API_KEY_MCE=environment.API_KEY_TINYMCE;

  //If needed to get the name of the template
  nameOfTemplate:string;
  //Two way databinding with the datamodel object in the tinyMCE-editor
  dataModel:string;
  
  //Not so relevant in this case, it is possible to add topics to the message
  topics:string[];
  //
  chosenSubjects:string[]=[];  


  constructor(private topicService: TopicsService, private messageTemplateService:MessageTemplateService,
    private router:Router, private ResponseTemplateService: ResponseTemplateService) {}

   ngOnInit(): void {

    this.ResponseTemplateService.curentDataModel.subscribe(message => this.dataModel=message)

     //Loading the topics from the database, the message is for now saved in topics
    this.topicService.getTopics()
    .subscribe(
      x=>{
        this.topics=x.map(x=>x.name);
      },
      err=>{
          console.log(err)
      }
    );
  }
   //Function for storing the selected technologies in an array,
   selectedTopics(event:any){
     console.log(this.nameOfTemplate)
     if(this.chosenSubjects.includes(event.target.value)){
       let index=this.chosenSubjects.indexOf(event.target.value);
      this.chosenSubjects.splice(index,1);
     }
     else{
      this.chosenSubjects.push(event.target.value);
     }
        }

   onFormSubmit(){
     console.log(this.nameOfTemplate)
    if(this.dataModel==null){
      this.dataModel="";
    }
    var jsonObject={
      "inputType": this.dataModel,
      "active": true,
      "communicationTemplateComponents": null
    }
    this.messageTemplateService.postMessageTemplates(jsonObject);
    this.router.navigateByUrl('/savedTemplates');
   }
   goToLibrary(){
     this.router.navigateByUrl('/savedTemplates');

   }
 
}
