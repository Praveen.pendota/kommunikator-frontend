import { Message } from './message';
import { Topic } from './topic.model';

export interface EmailMessage {
    Emails: string[];
    Topics: Topic[];
    UniqueLinks: string[],

    Subject: string;
    Message: Message;
  }