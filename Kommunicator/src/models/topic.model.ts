import { SubCategory } from './subcategory.model';
import { TopicSubscription } from './TopicSubscription.model';

export interface Topic {
    topicID?: number;
    id?: number,
    subCategoryID: number
    name: string,
    description: string,
    active?: boolean,
    subCategory?: SubCategory
    topicSubscriptions?: TopicSubscription[]
}