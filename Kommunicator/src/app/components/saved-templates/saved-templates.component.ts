import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MessageTemplateService } from 'src/app/services/message-template/message-template.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-saved-templates',
  templateUrl: './saved-templates.component.html',
  styleUrls: ['./saved-templates.component.scss']
})
export class SavedTemplatesComponent implements OnInit {

  constructor(private messageTemplateService:MessageTemplateService, private router:Router) { }

  public API_KEY_MCE=environment.API_KEY_TINYMCE;

  //List of saved templates loaded from the database
  @Input()  listOfSavedTemplates: any[] =[]
  //BOolean to show all the templates
  showAll: boolean=false;

  showListOfSavedTemplates: any[]=[]
  selectedTemplate:any;

  //Sending the data to the parent via eventemitter
  
  @Output() loadedEvent=new EventEmitter<any>();
  dataModel:any;
  
  ngOnInit(): void {
    this.messageTemplateService.curentDataModel.subscribe(message=>this.dataModel=message);
    this.messageTemplateService.getMessageTemplates().subscribe(
    x=>{
      this.listOfSavedTemplates=x.map(x=>new DOMParser().parseFromString(x.inputType,"text/html")
      );
    },
    err=>{
      console.log(err);
    });

  }
  loadEarlierTemplates(){
    this.showAll=!this.showAll;
    this.showListOfSavedTemplates=[];
    this.listOfSavedTemplates.map(
      element=>this.showListOfSavedTemplates.push(element.activeElement)
    );
    console.log(this.showListOfSavedTemplates)
  }
  
  /*youClickedMe(x){
    this.selectedTemplate="";
    this.messageTemplateService.setOutgoingTempateData(this.showListOfSavedTemplates[x]);
    this.selectedTemplate=this.messageTemplateService.getOutgoingTemplateData();
    console.log(this.showListOfSavedTemplates[x]);
    this.loadedEvent.emit(this.showListOfSavedTemplates[x]);
      }*/
}

