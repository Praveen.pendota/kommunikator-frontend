import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { FormsModule, FormControl,Validators,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {MatMenuModule} from '@angular/material/menu';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { EditorModule } from "@tinymce/tinymce-angular";
import {MatDialogModule} from '@angular/material/dialog';
import {MatRadioModule} from '@angular/material/radio';

import { AppComponent } from './app.component';
import { LogInPageComponent } from './pages/log-in-page/log-in-page.component';
import { AppHeaderComponent } from './shared/app-header/app-header.component';
import { AdminMainPageComponent } from './pages/admin-main-page/admin-main-page.component';
import { AppFooterComponent } from './shared/app-footer/app-footer.component';
import { ActionCardComponent } from './components/action-card/action-card.component';
import { CategorySidebarComponent } from './components/category-sidebar/category-sidebar.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { ManageTopicsComponent } from './pages/manage-topics/manage-topics.component';
import { EditTopicFormComponent } from './components/edit-topic-form/edit-topic-form.component';
import { EditCategoriesSidebarComponent } from './components/edit-categories-sidebar/edit-categories-sidebar.component';
import { TopicListComponent } from './components/topic-list/topic-list.component';
import { MessageTemplateComponent } from './components/message-template/message-template.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditDeleteDropdownComponent } from './components/edit-delete-dropdown/edit-delete-dropdown.component';
import { UserSubscriptionPageComponent } from './pages/user-subscription-page/user-subscription-page.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';
import { SavedMessageTemplatesComponent } from './components/saved-message-templates/saved-message-templates.component';
import { SelectedMessageTemplateComponent } from './components/selected-message-template/selected-message-template.component';
import { AdminReportComponent } from './pages/admin-report/admin-report.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { ResponseTemplateComponent } from './components/response-template/response-template.component';
import { OutgoingSetComponent } from './pages/outgoing-set/outgoing-set/outgoing-set.component';
import { ModalComponent } from './components/modal/modal.component';
import { PanelComponent } from './components/panel/panel.component';
import { DropDownOfCreatedMessagesComponent } from './components/drop-down-of-created-messages/drop-down-of-created-messages.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { PersonalSubscriptionsComponent } from './pages/personal-subscriptions/personal-subscriptions.component';
import { TopicCardComponent } from './components/topic-card/topic-card.component';
import { CreateTemplateComponent } from './pages/create-template/create-template.component';
import { TemplateForMessageComponent } from './components/template-for-message/template-for-message.component';
import { SavedTemplatesComponent } from './components/saved-templates/saved-templates.component';
import { MessageTemplateSidebarComponent } from './components/message-template-sidebar/message-template-sidebar.component';
import { ResponseTemplateSidebarComponent } from './components/response-template-sidebar/response-template-sidebar.component';
import { MessageTemplatesComponent } from './pages/message-templates/message-templates.component';
import { ExperimentalShowTemplatesComponent } from './pages/experimental-show-templates/experimental-show-templates.component';
import { CreateResponseTemplateComponent } from './pages/create-response-template/create-response-template.component';
import { ResponsePageComponent } from './pages/response-page/response-page.component';
import { CreateMessageFlowComponent } from './pages/create-message-flow/create-message-flow.component';
import { CreateMessageTemplateComponent } from './components/create-message-template/create-message-template.component';
import { OutgoingSetTopicSubscriptionsComponent } from './components/outgoing-set-topic-subscriptions/outgoing-set-topic-subscriptions.component';

// Initialize Keycloak
function initializeKeycloak(keycloak: KeycloakService) {
  return () => keycloak.init({
    config: {
      url: "https://auth-kommunicator.azurewebsites.net/auth",
      realm: "Auth",
      clientId: "authenticate",
    },
    initOptions: {
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri: window.location.origin + "/assets/silent-check-sso.html",
    },
  });
}

@NgModule({
  declarations: [
    AppComponent,
    LogInPageComponent,
    AppHeaderComponent,
    AdminMainPageComponent,
    AppFooterComponent,
    ActionCardComponent,
    CategorySidebarComponent,
    LandingPageComponent,
    ManageTopicsComponent,
    EditTopicFormComponent,
    EditCategoriesSidebarComponent,
    TopicListComponent,
    MessageTemplateComponent,
    UserSubscriptionPageComponent,
    ResponseTemplateComponent,
    SubscribeComponent,
    SavedMessageTemplatesComponent,
    SelectedMessageTemplateComponent,
    ResponseTemplateComponent,
    OutgoingSetComponent,
    ModalComponent,
    EditDeleteDropdownComponent,
    PanelComponent,
    BreadcrumbComponent,
    AdminReportComponent,
    DropDownOfCreatedMessagesComponent,
    PersonalSubscriptionsComponent,
    TopicCardComponent,
    CreateTemplateComponent,
    TemplateForMessageComponent,
    SavedTemplatesComponent,
    MessageTemplateSidebarComponent,
    ResponseTemplateSidebarComponent,
    CreateResponseTemplateComponent,
    ResponsePageComponent,
    CreateMessageFlowComponent,
    CreateMessageTemplateComponent,
    OutgoingSetTopicSubscriptionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    MatMenuModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    BrowserModule,
    EditorModule,
    Ng2SearchPipeModule,
    KeycloakAngularModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatDialogModule,
    MatRadioModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
