export interface KeycloakRestLogin {
    username: string;
    password: string;
    grant_type: string;
    client_id: string;
    client_secret: string;
  }
  
  export interface KeycloakAccessToken {
    access_token: string;
    expires_in: number;
    refresh_expires_in: number;
    refresh_token: string;
    token_type: string;
    'not-before-policy': number;
    session_state: string;
    scope: string;
  }