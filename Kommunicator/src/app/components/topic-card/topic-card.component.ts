import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Topic } from 'src/models/topic.model';

@Component({
  selector: 'app-topic-card',
  templateUrl: './topic-card.component.html',
  styleUrls: ['./topic-card.component.scss']
})
export class TopicCardComponent implements OnInit {

  @Input() topic: Topic;
  @Input() subCategoryName: string;
  @Input() categoryName: string;
  @Input() editable: boolean;
  @Output() editTopicEvent?: EventEmitter<any> = new EventEmitter<Topic>();
  @Output() deleteTopicEvent?: EventEmitter<any> = new EventEmitter<Topic>();

  constructor() { }

  ngOnInit(): void {
  }

  handleDeleteTopicClick() {
    this.deleteTopicEvent.emit(this.topic);
  }

  handleEditTopicClick() {
    this.editTopicEvent.emit(this.topic);
  }

}
