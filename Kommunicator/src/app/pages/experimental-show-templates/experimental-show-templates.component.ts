import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-experimental-show-templates',
  templateUrl: './experimental-show-templates.component.html',
  styleUrls: ['./experimental-show-templates.component.scss']
})
export class ExperimentalShowTemplatesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
