import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {

  authenticated: boolean = false;

  constructor(private authService: AuthenticationService) { }

  ngOnInit(): void {
    this.authService.authenticated().then(state => {
      this.authenticated = state;
    });
  }

  get Authenticated() {
    return this.authenticated;
  }

  logout() {
    this.authService.Logout();
  }
}
