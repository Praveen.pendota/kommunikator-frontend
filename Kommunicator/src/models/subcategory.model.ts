import { Category } from './category.model';
import { Topic } from './topic.model';

export interface SubCategory {
    id?: number,
    categoryID: number,
    name: string,
    active?: boolean,
    category?: Category,
    topics?: Topic[]
}