import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private auth: AuthenticationService, private http: HttpClient) {

  }

  private headers() : HttpHeaders {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    this.auth.GetToken()
    .then(token => {
      headers.append("Authorization", `Bearer ${token}`);
    });

    return headers;
  }

  get<T> (endpoint:string): Observable<T> {
    let headers =  this.headers();
    return this.http.get<T>(`https://kommunicator.azurewebsites.net/${endpoint}`, {
      headers: headers
    });
  }

  delete<T> (endpoint:string): Observable<T> {
    let headers =  this.headers();
    return this.http.delete<T>(`https://kommunicator.azurewebsites.net/${endpoint}`, {
      headers: headers
    });
  }

  post<T> (endpoint:string, payload: any) : Observable<T> {
    let headers =  this.headers();
    
    return this.http.post<T>(`https://kommunicator.azurewebsites.net/${endpoint}`, payload, {
      headers: headers
    });
  }

  put<T> (endpoint:string, payload: any) : Observable<T> {
    let headers =  this.headers();
    
    return this.http.put<T>(`https://kommunicator.azurewebsites.net/${endpoint}`, payload, {
      headers: headers
    });
  }
}
