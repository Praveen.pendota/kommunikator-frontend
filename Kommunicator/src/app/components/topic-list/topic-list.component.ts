import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/models/category.model';
import { SubCategory } from 'src/models/subcategory.model';
import { Topic } from 'src/models/topic.model';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.scss']
})
export class TopicListComponent implements OnInit {

  @Input() categories: Category[];
  @Input() subCategoryFilter: string;
  @Input() editable: boolean;

  @Output() editTopicEvent?: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteTopicEvent?: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  handleDeleteTopicEvent(topic: Topic, subCategory: SubCategory) {
    console.log(subCategory)
    this.deleteTopicEvent.emit({topic: topic, subCategory: subCategory});
  }

  handleEditTopicEvent(topic: Topic, subCategory: SubCategory) {
    this.editTopicEvent.emit({topic: topic, subCategory: subCategory});
  }

}
