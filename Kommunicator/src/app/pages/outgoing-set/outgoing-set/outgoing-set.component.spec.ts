import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgoingSetComponent } from './outgoing-set.component';

describe('OutgoingSetComponent', () => {
  let component: OutgoingSetComponent;
  let fixture: ComponentFixture<OutgoingSetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutgoingSetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgoingSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
